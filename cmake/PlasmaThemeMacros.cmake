#.rst:
# PlasmaThemeMacros
# ------------------
#
# This module provides the functions ``add_desktoptheme`` and ``add_desktoptheme_svgs``
# for generating Plasma theme bundles or directly installing a theme.
# 
# This file and the macros are for now private and should be copied, until they have matured
# enough to become official public macros part of KF5's Plasma macros.
#
# ::
#
#   add_desktoptheme(<target_name>
#       METADATA <file>
#       [NAME <name>] # defaults to 
#       [COLORS <colorsfile>]
#       [CUSTOM_FILES <file> [<file2> [...]]]
#       [VERSION <version>]  # defaults to PROJECT_VERSION
#       [CONFIGURE_METADATA]  # if metadata file should be processed for var substitution
#   )
#
#   add_desktoptheme_svgs(<target_name>
#       SUBPATH <relativepath>
#       FILES <file> [<file2> [...]]
#   )

#=============================================================================
# Copyright 2019 Friedrich W. H. Kossebau <kossebau@kde.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#=============================================================================


# needed to get PLASMA_DATA_INSTALL_DIR
find_package(KF5Plasma REQUIRED)

option(GZIP_DESKTOPTHEME_SVG "Install Desktop Theme SVG files as .svgz." ON)

if (GZIP_DESKTOPTHEME_SVG)
    find_package(gzip)
    set_package_properties(gzip PROPERTIES
        TYPE REQUIRED
    )
endif()

function(ADD_DESKTOPTHEME theme_target)
    set(options
        CONFIGURE_METADATA
    )
    set(oneValueArgs
        NAME
        VERSION
        COLORS
        METADATA
    )
    set(multiValueArgs
        CUSTOM_FILES
    )

    cmake_parse_arguments(PAD "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    if(NOT PAD_METADATA)
        message(FATAL_ERROR "METADATA needs to be defined when calling plasma_add_desktoptheme.")
    endif()

    if(NOT PAD_NAME)
        set(PAD_NAME ${theme_target})
    endif()

    if(NOT PAD_VERSION)
        set(PAD_VERSION ${PROJECT_VERSION})
    endif()

    set(desktoptheme_INSTALLDIR ${PLASMA_DATA_INSTALL_DIR}/desktoptheme/${PAD_NAME})

    set(${theme_target}_NAME "${PAD_NAME}")
    set(${theme_target}_VERSION "${PAD_VERSION}")
    set(bundlecontent_target "${theme_target}_bundle_content")
    set(bundle_target "${theme_target}_bundle")
    set(bundle_filename "${PAD_NAME}-${PAD_VERSION}.tar.gz")
    set(bundle_file "${CMAKE_CURRENT_BINARY_DIR}/${bundle_filename}")
    set(bundle_ROOTDIR "${CMAKE_CURRENT_BINARY_DIR}/${PAD_NAME}.bundle")
    set(bundle_DIR "${bundle_ROOTDIR}/${PAD_NAME}")

    # process files
    if(PAD_CONFIGURE_METADATA)
        set(PAD_METADATA_TEMPLATE "${PAD_METADATA}")
        set(PAD_METADATA "${CMAKE_CURRENT_BINARY_DIR}/metadata.desktop")
        configure_file(${PAD_METADATA_TEMPLATE} ${PAD_METADATA} @ONLY)
    endif()

    # install target
    add_custom_target(${theme_target} ALL)

    set_target_properties(${theme_target} PROPERTIES
        PLASMA_DESKTOPTHEME_NAME "${PAD_NAME}"
        PLASMA_DESKTOPTHEME_INSTALLDIR "${desktoptheme_INSTALLDIR}"
        PLASMA_DESKTOPTHEME_BUNDLECONTENTTARGET "${bundlecontent_target}"
        PLASMA_DESKTOPTHEME_BUNDLEDIR "${bundle_DIR}"
    )

    install(FILES
            ${PAD_COLORS}
            ${PAD_METADATA}
            ${PAD_CUSTOM_FILES}
        DESTINATION ${desktoptheme_INSTALLDIR}
    )

    # bundle target
    file(REMOVE_RECURSE "${bundle_DIR}") # make sure no old files are around on any config change
    file(MAKE_DIRECTORY "${bundle_DIR}") # TODO: create only on demand

    set(_bundled_files)
    foreach(_file
            ${PAD_COLORS}
            ${PAD_METADATA}
            ${PAD_CUSTOM_FILES})
        if (NOT IS_ABSOLUTE ${_file})
            set(_file "${CMAKE_CURRENT_SOURCE_DIR}/${_file}")
        endif()
        get_filename_component(_fileName "${_file}" NAME)
        set(_bundled_file "${bundle_DIR}/${_fileName}")
        add_custom_command(
            OUTPUT ${_bundled_file}
            COMMAND ${CMAKE_COMMAND}
            ARGS -E copy ${_file} ${_bundled_file}
            # trigger tar dep
            COMMAND ${CMAKE_COMMAND}
            ARGS -E touch ${bundle_DIR}
            DEPENDS ${_file}
        )

        list(APPEND _bundled_files "${_bundled_file}")
    endforeach()
    add_custom_target(${bundlecontent_target} DEPENDS ${_bundled_files})

    add_custom_command(
        OUTPUT ${bundle_file}
        COMMAND ${CMAKE_COMMAND}
        ARGS
            -E tar cfz ${bundle_file}
            -- ${bundle_DIR}
        WORKING_DIRECTORY "${bundle_ROOTDIR}"
        DEPENDS ${bundlecontent_target} ${bundle_DIR}
        COMMENT "Generating ${bundle_filename}"
    )
    add_custom_target(${bundle_target} DEPENDS ${bundle_file})

    # generic target for all bundles
    if (NOT TARGET desktoptheme-bundles)
        add_custom_target(desktoptheme-bundles)
    endif()
    add_dependencies(desktoptheme-bundles ${bundle_target})
    
    # make vars also accessible to callee scope
    set(${theme_target}_NAME "${theme_target}_NAME" PARENT_SCOPE)
    set(${theme_target}_VERSION "${theme_target}_VERSION" PARENT_SCOPE)

endfunction()

# Helper function, private for now
# Once it has matured and proven, add to public macros
function(ADD_DESKTOPTHEME_SVGS theme_target)
    set(options
    )
    set(oneValueArgs
        SUBPATH
    )
    set(multiValueArgs
        FILES
    )

    cmake_parse_arguments(PIDS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    if(NOT DEFINED PIDS_SUBPATH)
        message(FATAL_ERROR "SUBPATH needs to be defined when calling add_desktoptheme_svgs.")
    endif()

    if(NOT PIDS_FILES)
        message(FATAL_ERROR "No files passed when calling add_desktoptheme_svgs.")
    endif()

    if(NOT TARGET ${theme_target})
        message(FATAL_ERROR "No such target when calling add_desktoptheme_svgs: ${theme_target}.")
    endif()

    get_target_property(theme_name ${theme_target} PLASMA_DESKTOPTHEME_NAME)
    get_target_property(desktoptheme_INSTALLDIR ${theme_target} PLASMA_DESKTOPTHEME_INSTALLDIR)

    # process files
    if (GZIP_DESKTOPTHEME_SVG)
        set(_target_name "${theme_name}_desktoptheme_svgs_${PIDS_SUBPATH}")
        string(REPLACE "/" "_" _target_name "${_target_name}")

        set(desktoptheme_GZIPDIR "${theme_name}.gzipped/${PIDS_SUBPATH}")
        file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/${desktoptheme_GZIPDIR}")
    endif()

    set(_install_files)

    foreach(_src_file ${PIDS_FILES})
        if (NOT IS_ABSOLUTE ${_src_file})
            set(_src_file "${CMAKE_CURRENT_SOURCE_DIR}/${_src_file}")
        endif()
        if (NOT EXISTS ${_src_file})
            message(FATAL_ERROR "No such file found: ${_src_file}")
        endif()
        get_filename_component(_fileName "${_src_file}" NAME)

        if (GZIP_DESKTOPTHEME_SVG)
            set(_gzipped_file_displayname "${PIDS_SUBPATH}/${_fileName}z")
            set(_gzipped_file "${CMAKE_CURRENT_BINARY_DIR}/${desktoptheme_GZIPDIR}/${_fileName}z")
            add_custom_command(
                OUTPUT ${_gzipped_file}
                COMMAND ${gzip_EXECUTABLE}
                ARGS
                    -9
                    -c
                    ${_src_file} > ${_gzipped_file}
                DEPENDS ${_src_file}
                COMMENT "Gzipping ${_gzipped_file_displayname}"
            )
        else()
            set(_gzipped_file "${_src_file}")
        endif()

        list(APPEND _install_files "${_gzipped_file}")
    endforeach()

    # install target
    if (GZIP_DESKTOPTHEME_SVG)
        add_custom_target(${_target_name} ALL DEPENDS ${_install_files})
        add_dependencies(${theme_target} ${_target_name})
    endif()

    install(FILES ${_install_files} DESTINATION "${desktoptheme_INSTALLDIR}/${PIDS_SUBPATH}" )

    # bundle target
    get_target_property(bundlecontent_target ${theme_target} PLASMA_DESKTOPTHEME_BUNDLECONTENTTARGET)
    get_target_property(bundle_DIR ${theme_target} PLASMA_DESKTOPTHEME_BUNDLEDIR)

    set(_target_name "${theme_name}_desktoptheme_svgs_${PIDS_SUBPATH}")
    string(REPLACE "/" "_" _target_name "${_target_name}")

    set(bundle_svg_DIR "${bundle_DIR}/${PIDS_SUBPATH}")
    file(MAKE_DIRECTORY "${bundle_svg_DIR}") # TODO: create only on demand

    set(_bundled_files)
    foreach(_file ${_install_files})
        if (NOT IS_ABSOLUTE ${_file})
            set(_file "${CMAKE_CURRENT_SOURCE_DIR}/${_file}")
        endif()
        get_filename_component(_fileName "${_file}" NAME)
        set(_bundled_file "${bundle_svg_DIR}/${_fileName}")
        add_custom_command(
            OUTPUT ${_bundled_file}
            COMMAND ${CMAKE_COMMAND}
            ARGS -E copy ${_file} ${_bundled_file}
            # trigger tar dep
            COMMAND ${CMAKE_COMMAND}
            ARGS -E touch ${bundle_DIR}
            DEPENDS ${_file}
        )

        list(APPEND _bundled_files "${_bundled_file}")
    endforeach()

    set(_svg_bundlecontent_target "${bundlecontent_target}_svgs_${PIDS_SUBPATH}")
    string(REPLACE "/" "_" _svg_bundlecontent_target "${_svg_bundlecontent_target}")
    add_custom_target(${_svg_bundlecontent_target} DEPENDS ${_bundled_files})
    add_dependencies(${bundlecontent_target} ${_svg_bundlecontent_target})

endfunction()
